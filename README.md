This benchmark measures floating point performance against sine/cosine performance using AVX and AVX2 intrinsics, for different ratios between FMA and SINCOS.
