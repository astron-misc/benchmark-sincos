#include <string>
#include <iostream>
#include <iomanip>

#include "omp.h"

#define VECTOR_LENGTH 8
#define FMA 2
#define LENGTH 1024

float input[LENGTH] __attribute__((aligned(32)));
float output_sin[LENGTH] __attribute__((aligned(32)));
float output_cos[LENGTH] __attribute__((aligned(32)));

int vector_length() {
#if defined(__AVX512F__)
    return 16;
#elif defined(__AVX__)
    return 8;
#elif defined(__SSE__)
    return 4;
#elif defined(__ALTIVEC__)
    return 4;
#endif
}

void init()
{
    for (int i = 0; i < LENGTH; i++)
    {
        input[i] = 0.8;
    }
}

void kernel_1_0(int count);
void kernel_0_1(int count);
void kernel_1_1(int count);
void kernel_1_2(int count);
void kernel_1_4(int count);
void kernel_1_8(int count);
void kernel_1_16(int count);
void kernel_1_32(int count);
void kernel_1_64(int count);
void kernel_1_128(int count);
void kernel_1_256(int count);
void kernel_1_512(int count);

void report(std::string name, double runtime, double gflops, double gsincos)
{
    unsigned width = 8;
    std::cout << std::setw(width) << std::string(name) << ": ";
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << std::setw(width) << runtime * 1e3 << " ms";
    double gfmas = gflops / 2;
    double gops = gfmas + gsincos;
    std::cout << ", " << std::setw(width) << gflops / runtime << " GFLOPS/s";
    std::cout << ", " << std::setw(width) << gfmas / runtime << " GFMAS/s";
    std::cout << ", " << std::setw(width) << gsincos / runtime << " GSINCOS/s";
    std::cout << ", " << std::setw(width) << gops / runtime << " GOPS/s";
    std::cout << std::endl;
}

void run(const char *name, int nr_fma, int nr_sincos, void (*kernel)(int), bool print = true)
{
    unsigned long n = 8192;
    double runtime = 0;
    while (true)
    {
        runtime = -omp_get_wtime();
        kernel(n);
        runtime += omp_get_wtime();
        if (runtime < 0.5)
        {
            n *= 2;
        }
        else
        {
            break;
        }
    }
    double gflops = 1ULL * n * nr_fma * FMA * VECTOR_LENGTH * 1e-9;
    double gsincos = 1ULL * n * nr_sincos * VECTOR_LENGTH * 1e-9;
    if (print)
    {
        report(name, runtime, gflops, gsincos);
    }
}

int main()
{
    init();

    // Warmup
    run("", 0, 0, &kernel_1_0, false);

    // Benchmark
    run("  1:0", 2 << 10, 0, &kernel_1_0);
    run("512:1", 2 << 18, 2 << 9, &kernel_1_512);
    run("256:1", 2 << 17, 2 << 9, &kernel_1_256);
    run("128:1", 2 << 16, 2 << 9, &kernel_1_128);
    run(" 64:1", 2 << 15, 2 << 9, &kernel_1_64);
    run(" 32:1", 2 << 14, 2 << 9, &kernel_1_32);
    run(" 16:1", 2 << 13, 2 << 9, &kernel_1_16);
    run("  8:1", 2 << 12, 2 << 9, &kernel_1_8);
    run("  4:1", 2 << 11, 2 << 9, &kernel_1_4);
    run("  2:1", 2 << 10, 2 << 9, &kernel_1_2);
    run("  1:1", 2 << 9, 2 << 9, &kernel_1_1);
    run("  0:1", 0, 2 << 9, &kernel_0_1);
}
