#include <vec128int.h>
#include <vec128sp.h>

#define TWO_PI float(2 * M_PI)
#define NR_SAMPLES 8192

const unsigned two_pi_int = NR_SAMPLES;
const unsigned pi_int = two_pi_int / 2;
const unsigned hlf_pi_int = two_pi_int / 4;
float sin_lookup[NR_SAMPLES] __attribute__((aligned(32)));

inline void SINCOS_1024()
{
    for (unsigned i = 0; i < LENGTH; i += 4) {
        __m128  f0 = vec_load4sp(&input[i]);          // input
        __m128  f1 = vec_splat4sp(two_pi_int/TWO_PI); // compute scale
        __m128  f2 = vec_multiply4sp(f0, f1);         // apply scale
        __m128i u0 = vec_splat4sw(hlf_pi_int);        // constant 0.5 * pi
        __m128i u1 = vec_splat4sw(two_pi_int - 1);    // mask 2 * pi
        __m128i u2 = vec_convert4spto4sw(f2);         // round float to int
        __m128i u3 = vec_add(u2, u0);                 // add 0.5 * pi
        __m128i u4 = vec_bitand1q(u1, u3);            // apply mask of 2 * pi, second index
        __m128i u5 = vec_bitand1q(u1, u2);            // apply mask of 2 * pi, first index
        __m128  f3 = vec_gather4sp(sin_lookup, u4);   // perform lookup of real
        __m128  f4 = vec_gather4sp(sin_lookup, u5);   // perform lookup of imag
        vec_store4sp(&output_sin[i], f3);
        vec_store4sp(&output_cos[i], f4);
    }
}