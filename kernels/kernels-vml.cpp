#include "kernels.hpp"
#include "intel_intrinsics_fma.h"

#define VML_PRECISION VML_LA
#include <mkl_vml.h>

inline void SINCOS_1024()
{
    vmsSinCos(1024, input, output_sin, output_cos, VML_PRECISION);
}
