void FMA_8();
void SINCOS_1024();

#define LENGTH 1024
extern float input[LENGTH];
extern float output_sin[LENGTH];
extern float output_cos[LENGTH];

void kernel_1_0(int n)
{
#pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < n; i++)
    {
        for (unsigned long j = 0; j < 256; j++)
        {
            FMA_8();
        }
    }
}

void kernel_0_1(int n)
{
#pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < n; i++)
    {
        SINCOS_1024();
    }
}

template <int fma>
void kernel_sincos_fma(int n)
{
#pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < n; i++)
    {
        SINCOS_1024();
        for (unsigned long j = 0; j < (1024 / 8 * fma); j++)
        {
            FMA_8();
        }
    }
}

void kernel_1_1(int n) { kernel_sincos_fma<1>(n); }

void kernel_1_2(int n) { kernel_sincos_fma<2>(n); }

void kernel_1_4(int n) { kernel_sincos_fma<4>(n); }

void kernel_1_8(int n) { kernel_sincos_fma<8>(n); }

void kernel_1_16(int n) { kernel_sincos_fma<16>(n); }

void kernel_1_32(int n) { kernel_sincos_fma<32>(n); }

void kernel_1_64(int n) { kernel_sincos_fma<64>(n); }

void kernel_1_128(int n) { kernel_sincos_fma<128>(n); }

void kernel_1_256(int n) { kernel_sincos_fma<256>(n); }

void kernel_1_512(int n) { kernel_sincos_fma<512>(n); }
