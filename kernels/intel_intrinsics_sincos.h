#include <immintrin.h>

#if defined(__AVX512__)
inline void SINCOS_8()
{
    a1 = _mm512_sincos_ps(&a1, a1);
    b1 = _mm512_sincos_ps(&b1, b1);
    c1 = _mm512_sincos_ps(&c1, c1);
    d1 = _mm512_sincos_ps(&d1, d1);
    e1 = _mm512_sincos_ps(&e1, e1);
    f1 = _mm512_sincos_ps(&f1, f1);
    g1 = _mm512_sincos_ps(&g1, g1);
    h1 = _mm512_sincos_ps(&h1, h1);
}
#elif defined(__AVX__)
inline void SINCOS_8()
{
    a1 = _mm256_sincos_ps(&a1, a1);
    b1 = _mm256_sincos_ps(&b1, b1);
    c1 = _mm256_sincos_ps(&c1, c1);
    d1 = _mm256_sincos_ps(&d1, d1);
    e1 = _mm256_sincos_ps(&e1, e1);
    f1 = _mm256_sincos_ps(&f1, f1);
    g1 = _mm256_sincos_ps(&g1, g1);
    h1 = _mm256_sincos_ps(&h1, h1);
}
#else
#error "You need at least AVX to run this benchmark."
#endif
