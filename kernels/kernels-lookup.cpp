#include <math.h>

#include "kernels.hpp"
#if defined(__x86_64__)
#include "intel_intrinsics_fma.h"
#include "intel_intrinsics_lookup.h"
#elif defined(__powerpc__)
#include "powerpc_intrinsics_fma.h"
#include "powerpc_intrinsics_lookup.h"
#else
#error "Unsupported architecture."
#endif
