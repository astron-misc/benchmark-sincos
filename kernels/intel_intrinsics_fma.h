#include <immintrin.h>

#if defined(__AVX512__)
__m512 a1, b1, c1, d1, e1, f1, g1, h1;
inline void FMA_8()
{
    a1 = _mm512_fmadd_ps(a1, a1, a1);
    b1 = _mm512_fmadd_ps(b1, b1, b1);
    c1 = _mm512_fmadd_ps(c1, c1, c1);
    d1 = _mm512_fmadd_ps(d1, d1, d1);
    e1 = _mm512_fmadd_ps(e1, e1, e1);
    f1 = _mm512_fmadd_ps(f1, f1, f1);
    g1 = _mm512_fmadd_ps(g1, g1, g1);
    h1 = _mm512_fmadd_ps(h1, h1, h1);
}
#elif defined(__AVX__) && defined(__FMA__)
__m256 a1, b1, c1, d1, e1, f1, g1, h1;
inline void FMA_8()
{
    a1 = _mm256_fmadd_ps(a1, a1, a1);
    b1 = _mm256_fmadd_ps(b1, b1, b1);
    c1 = _mm256_fmadd_ps(c1, c1, c1);
    d1 = _mm256_fmadd_ps(d1, d1, d1);
    e1 = _mm256_fmadd_ps(e1, e1, e1);
    f1 = _mm256_fmadd_ps(f1, f1, f1);
    g1 = _mm256_fmadd_ps(g1, g1, g1);
    h1 = _mm256_fmadd_ps(h1, h1, h1);
}
#elif defined(__AVX__)
__m256 a1, b1, c1, d1, e1, f1, g1, h1;
inline void FMA_8()
{
    a1 = _mm256_add_ps(a1, _mm256_mul_ps(a1, a1));
    b1 = _mm256_add_ps(b1, _mm256_mul_ps(b1, b1));
    c1 = _mm256_add_ps(c1, _mm256_mul_ps(c1, c1));
    d1 = _mm256_add_ps(d1, _mm256_mul_ps(d1, d1));
    e1 = _mm256_add_ps(e1, _mm256_mul_ps(e1, e1));
    f1 = _mm256_add_ps(f1, _mm256_mul_ps(f1, f1));
    g1 = _mm256_add_ps(g1, _mm256_mul_ps(g1, g1));
    h1 = _mm256_add_ps(h1, _mm256_mul_ps(h1, h1));
}
#else
#error "You need at least AVX to run this benchmark."
#endif
