#include "kernels.hpp"
#include "intel_intrinsics_fma.h"
#include "intel_intrinsics_sincos.h"

inline void SINCOS_1024()
{
    for (unsigned i = 0; i < LENGTH; i += 8)
    {
        SINCOS_8();
    }
}
