#include <altivec.h>

vector float a1, b1, c1, d1, e1, f1, g1, h1;

inline void FMA_8()
{
    a1 = vec_add(a1, vec_mul(a1, a1));
    b1 = vec_add(b1, vec_mul(b1, b1));
    c1 = vec_add(c1, vec_mul(c1, c1));
    d1 = vec_add(d1, vec_mul(d1, d1));
    e1 = vec_add(e1, vec_mul(e1, e1));
    f1 = vec_add(f1, vec_mul(f1, f1));
    g1 = vec_add(g1, vec_mul(g1, g1));
    h1 = vec_add(h1, vec_mul(h1, h1));
}
