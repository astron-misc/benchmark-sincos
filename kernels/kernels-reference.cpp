#include <math.h>

#include "kernels.hpp"
#if defined(__x86_64__)
#include "intel_intrinsics_fma.h"
#elif defined(__powerpc__)
#include "powerpc_intrinsics_fma.h"
#else
#error "Unsupported architecture."
#endif

inline void SINCOS_1024()
{
    for (unsigned i = 0; i < LENGTH; i++)
    {
        output_sin[i] = sinf(input[i]);
    }
    for (unsigned i = 0; i < LENGTH; i++)
    {
        output_cos[i] = cosf(input[i]);
    }
}
