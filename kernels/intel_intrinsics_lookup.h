#include <immintrin.h>

#define TWO_PI float(2 * M_PI)
#define NR_SAMPLES 8192

const unsigned two_pi_int = NR_SAMPLES;
const unsigned pi_int = two_pi_int / 2;
const unsigned hlf_pi_int = two_pi_int / 4;
float sin_lookup[NR_SAMPLES] __attribute__((aligned(32)));

#if defined(__AVX__)
inline void SINCOS_1024()
{
    __m256 two_pi_f = _mm256_set1_ps(TWO_PI);
    __m256 two_pi_inv_f = _mm256_set1_ps(1 / TWO_PI);
    __m256 two_pi_int_f = _mm256_set1_ps(two_pi_int);
    __m256 scale_f = _mm256_mul_ps(two_pi_int_f, two_pi_inv_f);
    __m256i hlf_pi_int_i = _mm256_set1_epi32(hlf_pi_int);
    __m256i mask_i = _mm256_set1_epi32(two_pi_int - 1);

    for (unsigned i = 0; i < 1024; i += 8)
    {
        __m256 f0 = _mm256_load_ps(&input[i]);       // load input
        __m256 f1 = _mm256_mul_ps(f0, two_pi_inv_f); // divide input by 2 * pi
        __m256i i0 = _mm256_cvtps_epi32(f1);         // get integer part
        __m256 f2 = _mm256_cvtepi32_ps(i0);          // convert to float
#if defined(__AVX2__)
        __m256 f4 = _mm256_fnmadd_ps(f2, two_pi_f, f0);
#else
        __m256 f3 = _mm256_mul_ps(f2, two_pi_f);      // get multiple of 2 * pi
        __m256 f4 = _mm256_sub_ps(f0, f3);            // normalize input
#endif
        __m256 f5 = _mm256_mul_ps(f4, scale_f);          // apply scale
        __m256i i1 = _mm256_cvtps_epi32(f5);             // convert to int
        __m256i i2 = _mm256_add_epi32(i1, hlf_pi_int_i); // shift by 0.5 * pi
        __m256i i3 = _mm256_and_si256(i2, mask_i);       // apply mask, first index
        __m256i i4 = _mm256_and_si256(i1, mask_i);       // apply mask, second index
#if defined(__AVX2__)
        __m256 f6 = _mm256_i32gather_ps(sin_lookup, i3, 4); // lookup cosine
        __m256 f7 = _mm256_i32gather_ps(sin_lookup, i4, 4); // lookup sine
#else
        __m256 f6 = _mm256_gather_ps(sin_lookup, i3); // lookup cosine
        __m256 f7 = _mm256_gather_ps(sin_lookup, i4); // lookup sine
#endif
        _mm256_store_ps(&output_sin[i], f6);
        _mm256_store_ps(&output_cos[i], f7);
    }
}
#else
#error "You need at least AVX to run this benchmark."
#endif
